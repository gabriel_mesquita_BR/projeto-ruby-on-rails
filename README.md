# SISTEMA BANCÁRIO #

## VERSÕES

### RUBY 2.6.6
### RAILS 6.0.3

## BANCO DE DADOS

### POSTGRESQL

## BIBLIOTECA PARA TESTES

### MINITEST

## PASSOS PARA A EXECUÇÃO DO PROJETO

* git clone https://gabriel_mesquita_BR@bitbucket.org/gabriel_mesquita_BR/projeto-ruby-on-rails.git
* Acesse o diretório Sistema_de_controle_Bancario
* bundle install
* yarn install --check-files
* coloque a senha do seu banco de dados para a variável de ambiente nomeada de SISTEMABANCARIO_BD_SENHA para o ambiente de desenvolvimento
* coloque a senha do seu banco de dados para a variável de ambiente nomeada de SISTEMABANCARIOTESTES_BD_SENHA para o ambiente de testes
* rails s

## VARIÁVEL DE AMBIENTE

### WINDOWS
* set SISTEMABANCARIO_BD_SENHA=senha_do_seu_bd
* set SISTEMABANCARIOTESTES_BD_SENHA=senha_do_seu_bd