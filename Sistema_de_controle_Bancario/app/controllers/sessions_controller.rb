class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])

    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:notice] = "Bem-vindo #{user.username}!"
      redirect_to root_path
    else
      flash.now[:alert] = 'Há algo errado com os seus dados de acesso!'
      render 'new'
    end
  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = 'Volte sempre!'
    redirect_to root_path
  end
end
