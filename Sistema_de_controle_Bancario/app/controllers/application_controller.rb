class ApplicationController < ActionController::Base
  before_action :set_locale

  unless ActionController::Base.config.consider_all_requests_local
    rescue_from Exception, with: :server_error
    rescue_from ActiveRecord::RecordNotFound, with: :not_found
  end

  helper_method :current_user, :logged_in?

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end

  def require_user
    if !logged_in?
      flash[:alert] = 'Você deve estar logado para realizar esta ação!'
      redirect_to acesso_path
    end
  end

  private

  def not_found
    render template: '/errors/404', status: 404
  end

  def server_error
    render template: '/errors/500', status: 500
  end

  def set_locale
    I18n.locale = 'pt-BR'
  end
end
