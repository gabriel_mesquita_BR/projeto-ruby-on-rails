class BankAccountsController < ApplicationController
  before_action :set_bank_account, only: [:show, :edit, :update, :destroy]
  before_action :require_user

  def index
    @bank_accounts = BankAccount.paginate(page: params[:page], per_page: 5)
  end

  def new
    @bank_account = BankAccount.new
  end

  def create
    @bank_account = BankAccount.new(bank_account_params)

    if @bank_account.save
      flash[:notice] = 'Conta bancária criada com sucesso!'
      redirect_to new_bank_account_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @bank_account.update(bank_account_params)
      flash[:notice] = 'Conta bancária atualizada com sucesso!'
      redirect_to bank_account_path(@bank_account)
    else
      render 'edit'
    end
  end

  def destroy
    @bank_account.destroy
    flash[:notice] = 'Conta bancária removida com sucesso!'
    redirect_to bank_accounts_path
  end

  private

  def set_bank_account
    @bank_account = BankAccount.find(params[:id])
  end

  def bank_account_params
    params.require(:bank_account).permit(:account_number, :limit, :bank_agency_id)
  end
end
