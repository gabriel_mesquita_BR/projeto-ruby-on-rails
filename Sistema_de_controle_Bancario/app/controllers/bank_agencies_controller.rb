class BankAgenciesController < ApplicationController
  before_action :set_bank_agency, only: [:show, :edit, :update, :destroy]
  before_action :require_user

  def index
    @bank_agencies = BankAgency.paginate(page: params[:page], per_page: 5)
  end

  def new
    @bank_agency = BankAgency.new
  end

  def create
    @bank_agency = BankAgency.new(bank_agency_params)

    if @bank_agency.save
      flash[:notice] = 'Agência bancária criada com sucesso!'
      redirect_to new_bank_agency_path
    else
      render 'new'
    end
  end

  def show
    @bank_accounts = @bank_agency.bank_accounts.paginate(page: params[:page], per_page: 5)
  end

  def edit
  end

  def update
    if @bank_agency.update(bank_agency_params)
      flash[:notice] = 'Agência bancária atualizada com sucesso!'
      redirect_to bank_agency_path(@bank_agency)
    else
      render 'edit'
    end
  end

  def destroy
    if @bank_agency.destroy
      flash[:notice] = 'Agência bancária removida com sucesso!'
    else
      flash[:alert] = "#{@bank_agency.name} não pode ser removida, pois possui contas bancárias!"
    end
    redirect_to bank_agencies_path
  end

  private

  def set_bank_agency
    @bank_agency = BankAgency.find(params[:id])
  end

  def bank_agency_params
    params.require(:bank_agency).permit(:name, :number, :address, :image)
  end
end
