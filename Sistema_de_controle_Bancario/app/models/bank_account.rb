class BankAccount < ApplicationRecord
  belongs_to :bank_agency
  validates :account_number, presence: true, uniqueness: { case_sensitive: false }
  validates :limit, presence: true
end
