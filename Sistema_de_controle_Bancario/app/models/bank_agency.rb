class BankAgency < ApplicationRecord
  has_many :bank_accounts, dependent: :restrict_with_error
  has_one_attached :image

  validates :name, presence: true
  validates :number, presence: true, uniqueness: { case_sensitive: false }
  validates :address, presence: true
  validates :image, presence: true
  validate :acceptable_image

  def acceptable_image
    return unless image.attached?

    if image.byte_size > 1.megabyte
      errors.add(:image, 'deve ser de até 1 mb')
    end

    acceptable_types = ['image/jpeg', 'image/png']
    unless acceptable_types.include?(image.content_type)
      errors.add(:image, 'deve ser JPEG ou PNG')
    end
  end
end
