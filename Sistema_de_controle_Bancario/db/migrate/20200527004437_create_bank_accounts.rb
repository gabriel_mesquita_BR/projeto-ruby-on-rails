class CreateBankAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :bank_accounts do |t|
      t.string :account_number
      t.string :limit
      t.integer :bank_agency_id
    end
  end
end
