class AddColumnNameToBankAgencies < ActiveRecord::Migration[6.0]
  def change
    add_column :bank_agencies, :name, :string
  end
end
