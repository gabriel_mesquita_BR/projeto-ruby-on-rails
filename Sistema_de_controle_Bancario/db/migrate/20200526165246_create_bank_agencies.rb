class CreateBankAgencies < ActiveRecord::Migration[6.0]
  def change
    create_table :bank_agencies do |t|
      t.string :number
      t.text :address
    end
  end
end
