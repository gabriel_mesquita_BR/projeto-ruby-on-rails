Rails.application.routes.draw do
  root 'pages#home'

  scope(path_names: { new: 'novo', edit: 'editar' }) do
    resources :bank_agencies, path: 'agencias'
  end

  scope(path_names: { new: 'novo', edit: 'editar' }) do
    resources :bank_accounts, path: 'contas'
  end

  scope(path_names: { new: 'registro', edit: 'editar' }) do
    resources :users, except: [:index], path: 'usuarios'
  end

  get 'acesso', to: 'sessions#new'
  post 'acesso', to: 'sessions#create'
  delete 'sair', to: 'sessions#destroy'
end
