require 'test_helper'

class BankAgencyTest < ActiveSupport::TestCase
  test 'should be valid user' do
    @user = User.new(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
    assert @user.valid?
  end

  test 'should be present username' do
    @user = User.new(username: '')
    assert_not @user.valid?
  end

  test 'should be present email' do
    @user = User.new(email: '')
    assert_not @user.valid?
  end

  test 'should be valid email' do
    @user = User.new(email: 'nome1@gmail')
    assert_not @user.valid?
  end

  test 'should be present password' do
    @user = User.new(password_digest: '')
    assert_not @user.valid?
  end
end
