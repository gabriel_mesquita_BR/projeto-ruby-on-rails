require 'test_helper'

class BankAccountTest < ActiveSupport::TestCase
  test 'should be valid bank_account' do
    @bank_agency = BankAgency.create(name: 'Agência 1', number: '1234', address: 'rua do banco')
    @bank_account = BankAccount.new(account_number: '12345678-9', limit: '500', bank_agency_id: @bank_agency.id)
    assert @bank_account.valid?
  end

  test 'should be present account_number' do
    @bank_account = BankAccount.new(account_number: '')
    assert_not @bank_account.valid?
  end

  test 'should be unique account_number' do
    @bank_account = BankAccount.new(account_number: '12345678-9')
    @bank_account.save

    @bank_account2 = BankAccount.new(account_number: '12345678-9')
    assert_not @bank_account2.valid?
  end

  test 'should be present limit' do
    @bank_account = BankAccount.new(limit: '')
    assert_not @bank_account.valid?
  end
end
