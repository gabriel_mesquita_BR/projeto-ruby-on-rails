require 'test_helper'

class BankAgencyTest < ActiveSupport::TestCase
  test 'should be valid bank_agency' do
    @bank_agency = BankAgency.new(name: 'Agência 1', number: '1234', address: 'rua do banco')
    assert @bank_agency.valid?
  end

  test 'should be present name' do
    @bank_agency = BankAgency.new(name: '')
    assert_not @bank_agency.valid?
  end

  test 'should be present number' do
    @bank_agency = BankAgency.new(number: '')
    assert_not @bank_agency.valid?
  end

  test 'should be unique number' do
    @bank_agency = BankAgency.new(number: '1234')
    @bank_agency.save

    @bank_agency2 = BankAgency.new(number: '1234')
    assert_not @bank_agency2.valid?
  end

  test 'should be present address' do
    @bank_agency = BankAgency.new(address: '')
    assert_not @bank_agency.valid?
  end
end
