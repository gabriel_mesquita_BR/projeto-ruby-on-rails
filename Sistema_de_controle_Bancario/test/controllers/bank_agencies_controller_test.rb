require 'test_helper'

class BankAgenciesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @bank_agency = BankAgency.create(name: 'Agência 1', number: '1234', address: 'rua do banco')
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'should get index' do
    sign_in_as(@user, 'senhaaleatoria')
    get bank_agencies_url
    assert_response :success
  end

  test 'should get new' do
    sign_in_as(@user, 'senhaaleatoria')
    get new_bank_agency_url
    assert_response :success
  end

  test 'should create bank_agency' do
    sign_in_as(@user, 'senhaaleatoria')
    assert_difference('BankAgency.count', 1) do
      post bank_agencies_url, params: { bank_agency: { name: 'Agência 2', number: '2345', address: 'rua do banco 2345' } }
    end

    assert_redirected_to new_bank_agency_url
  end

  test 'should show bank_agency' do
    sign_in_as(@user, 'senhaaleatoria')
    get bank_agency_url(@bank_agency)
    assert_response :success
  end

  test 'should get edit' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_bank_agency_url(@bank_agency)
    assert_response :success
  end

  test 'should update bank_agency' do
    sign_in_as(@user, 'senhaaleatoria')
    patch bank_agency_url(@bank_agency), params: { bank_agency: { name: 'Agência 2', number: '2345', address: 'avenida do banco 2345' } }
    assert_redirected_to bank_agency_url(@bank_agency)
  end

  test 'should destroy bank_agency' do
    sign_in_as(@user, 'senhaaleatoria')
    assert_difference('BankAgency.count', -1) do
      delete bank_agency_url(@bank_agency)
    end

    assert_redirected_to bank_agencies_url
  end
end
