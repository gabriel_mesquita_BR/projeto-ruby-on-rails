require 'test_helper'

class BankAccountsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @bank_agency = BankAgency.create(name: 'Agência 1', number: '2345', address: 'avenida do banco 2345')
    @bank_account = BankAccount.create(account_number: '12345678-9', limit: '500,45', bank_agency_id: @bank_agency.id)
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'should get index' do
    sign_in_as(@user, 'senhaaleatoria')
    get bank_accounts_url
    assert_response :success
  end

  test 'should get new' do
    sign_in_as(@user, 'senhaaleatoria')
    get new_bank_account_url
    assert_response :success
  end

  test 'should create bank_account' do
    sign_in_as(@user, 'senhaaleatoria')
    assert_difference('BankAccount.count', 1) do
      post bank_accounts_url, params: { bank_account: { account_number: '456894', limit: '126,57', bank_agency_id: @bank_agency.id } }
    end

    assert_redirected_to new_bank_account_url
  end

  test 'should show account' do
    sign_in_as(@user, 'senhaaleatoria')
    get bank_account_url(@bank_account)
    assert_response :success
  end

  test 'should get edit' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_bank_account_url(@bank_account)
    assert_response :success
  end

  test 'should update bank_account' do
    sign_in_as(@user, 'senhaaleatoria')
    patch bank_account_url(@bank_account), params: { bank_account: { account_number: '456894', limit: '926,57',
      bank_agency_id: @bank_agency.id } }
    assert_redirected_to bank_account_url(@bank_account)
  end

  test 'should destroy bank_account' do
    sign_in_as(@user, 'senhaaleatoria')
    assert_difference('BankAccount.count', -1) do
      delete bank_account_url(@bank_account)
    end

    assert_redirected_to bank_accounts_url
  end
end
