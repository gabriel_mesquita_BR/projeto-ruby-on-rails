require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'should get new' do
    get new_user_url
    assert_response :success
  end

  test 'should create user' do
    assert_difference('User.count', 1) do
      post users_url, params: { user: { username: 'Nome 2', email: 'nome2@gmail.com', password: 'senhaaleatoria' } }
    end

    assert_redirected_to root_url
  end

  test 'should show user' do
    sign_in_as(@user, 'senhaaleatoria')
    get user_url(@user)
    assert_response :success
  end

  test 'should get edit' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_user_url(@user)
    assert_response :success
  end

  test 'should update user' do
    sign_in_as(@user, 'senhaaleatoria')
    patch user_url(@user), params: { user: { username: 'Nome 1', email: 'nomesobrenome@gmail.com', password: 'senhaaleatoria' } }
    assert_redirected_to root_url
  end

  test 'should destroy user' do
    sign_in_as(@user, 'senhaaleatoria')
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to root_url
  end
end
