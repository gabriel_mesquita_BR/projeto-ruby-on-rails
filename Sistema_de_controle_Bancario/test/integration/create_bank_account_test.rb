require 'test_helper'

class CreateBankAccountTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'get form of new account and create account' do
    sign_in_as(@user, 'senhaaleatoria')
    @bank_agency = BankAgency.create(name: 'Agência 1', number: '3456', address: 'avenida do banco 3456')

    get new_bank_account_url
    assert_response :success
    assert_difference('BankAccount.count', 1) do
      post bank_accounts_url, params: { bank_account: { account_number: '456894', limit: '926,57', bank_agency_id: @bank_agency.id } }
      assert_response :redirect
    end
    follow_redirect!
    assert_response :success
  end

  test 'get form of new account and reject invalid account' do
    sign_in_as(@user, 'senhaaleatoria')
    get new_bank_account_url
    assert_response :success
    assert_no_difference('BankAccount.count', 1) do
      post bank_accounts_url, params: { bank_account: { account_number: '', limit: '', bank_agency_id: 10 } }
    end

    assert_match 'erros', response.body
    assert_select 'div.alert'
    assert_select 'h4.alert-heading'
  end
end
