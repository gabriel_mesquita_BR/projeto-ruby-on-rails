require 'test_helper'

class UpdateUserTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'get form of edit user and update user' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_user_url(@user)
    assert_response :success
    patch user_url(@user), params: { user: { username: 'Nome 1', email: 'nome1sobrenome@gmail.com', password: 'senhaaleatoria' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end

  test 'get form of edit user and reject invalid user' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_user_url(@user)
    assert_response :success
    patch user_url(@user), params: { user: { username: 'Nome 1', email: 'nome1sobrenome@gmail', password: 'senhaaleatoria' } }
    assert_match 'erros', response.body
    assert_select 'div.alert'
    assert_select 'h4.alert-heading'
  end
end
