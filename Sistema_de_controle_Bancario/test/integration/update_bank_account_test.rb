require 'test_helper'

class UpdateBankAccountTest < ActionDispatch::IntegrationTest
  def setup
    @bank_agency = BankAgency.create(name: 'Agência 1', number: '1234', address: 'rua do banco')
    @bank_account = BankAccount.create(account_number: '106745', limit: '926,57', bank_agency_id: @bank_agency.id)
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'get form of edit account and update account' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_bank_account_url(@bank_account)
    assert_response :success
    patch bank_account_url(@bank_account), params: { bank_account: { account_number: '106745', limit: '1.256,57',
      bank_agency_id: @bank_agency.id } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end

  test 'get form of edit account and reject invalid account' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_bank_account_url(@bank_account)
    assert_response :success
    patch bank_account_url(@bank_account), params: { bank_account: { account_number: '', limit: '',
      bank_agency_id: 25} }
    assert_match 'erros', response.body
    assert_select 'div.alert'
    assert_select 'h4.alert-heading'
  end
end
