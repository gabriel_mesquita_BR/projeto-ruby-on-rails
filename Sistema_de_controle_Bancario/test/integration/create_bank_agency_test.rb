require 'test_helper'

class CreateBankAgencyTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'get form of new agency and create agency' do
    sign_in_as(@user, 'senhaaleatoria')
    get new_bank_agency_url
    assert_response :success
    assert_difference('BankAgency.count', 1) do
      post bank_agencies_url, params: { bank_agency: {name: 'Agência 1', number: '2345', address: 'rua do banco 2345' } }
      assert_response :redirect
    end
    follow_redirect!
    assert_response :success
  end

  test 'get form of new agency and reject invalid agency' do
    sign_in_as(@user, 'senhaaleatoria')
    get new_bank_agency_url
    assert_response :success
    assert_no_difference('BankAgency.count', 1) do
      post bank_agencies_url, params: { bank_agency: {name: '', number: '', address: '' } }
    end
    assert_match 'erros', response.body
    assert_select 'div.alert'
    assert_select 'h4.alert-heading'
  end
end
