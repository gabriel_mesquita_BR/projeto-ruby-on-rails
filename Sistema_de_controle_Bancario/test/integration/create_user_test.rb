require 'test_helper'

class CreateUserTest < ActionDispatch::IntegrationTest

  test 'get form of new user and create user' do
    get new_user_url
    assert_response :success
    assert_difference('User.count', 1) do
      post users_url, params: { user: { username: 'Nome 2', email: 'nome2@gmail.com', password: 'senhaaleatoria' } }
      assert_response :redirect
    end
    follow_redirect!
    assert_response :success
  end

  test 'get form of new user and reject invalid user' do
    get new_user_url
    assert_response :success
    assert_no_difference('BankAccount.count', 1) do
      post bank_accounts_url, params: { user: { username: 'Nome 2', email: 'nome2@gmail', password: 'senhaaleatoria' } }
    end
  end
end
