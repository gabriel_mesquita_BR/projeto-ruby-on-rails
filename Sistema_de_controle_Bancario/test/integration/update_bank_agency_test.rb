require 'test_helper'

class UpdateBankAgencyTest < ActionDispatch::IntegrationTest
  def setup
    @bank_agency = BankAgency.create(name: 'Agência 1', number: '1234', address: 'rua do banco')
    @user = User.create(username: 'Nome 1', email: 'nome1@gmail.com', password: 'senhaaleatoria')
  end

  test 'get form of edit agency and update agency' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_bank_agency_url(@bank_agency)
    assert_response :success
    patch bank_agency_url(@bank_agency), params: { bank_agency: {name: 'Agência 2', number: '2345', 
      address: 'avenida do banco 2345, ap. 3' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end

  test 'get form of edit agency and reject invalid agency' do
    sign_in_as(@user, 'senhaaleatoria')
    get edit_bank_agency_url(@bank_agency)
    assert_response :success
    patch bank_agency_url(@bank_agency), params: { bank_agency: {name: '', number: '', address: 'avenida do banco 2345, ap. 3' } }
    assert_match 'erros', response.body
    assert_select 'div.alert'
    assert_select 'h4.alert-heading'
  end
end
